## Instruções Gerais

O objetivo deste teste é analisar o seu conhecimento em desenvolvimento front-end usando recursos específicos.

## Instruções Gerais

- Siga todos os passos descritos abaixo:
- O projeto final precisa ser publicado na sua conta do BitBucket;
- O link com o teste finalizado precisa ser enviado para o seguinte email [vagas@indexdigital.com.br](mailto:vagas@indexdigital.com.br).

## Features

- Desenvolva um ** Quiz ** usando React;
- O usuário poderá criar um quiz, perguntas e respostas;
- O quiz pode ter diversas perguntas;
- Uma pergunta pode ser múltiplas respostas;
- Ao escolher um quiz, o usuário será redirecionado para a página que listarará todas as perguntas daquele quiz com as respectivas respostas;
- Ao responder uma pergunta, você deverá informar se aquela escolha foi correta ou errada;
- Cada pergunta deverá ter pelo menos 4 respostas, sendo uma delas a resposta correta;
- NPM ou Yarn devem ser utilizados para gerenciar as dependências;
- Crie um README com as instruções de instalação da aplicação;
- Escolha uma tema (Bootstrap, Foundation...);
- User bibliotecas de terceiros;
- Escolha como você salvará as respostas (local storage, database, json...);

## Diferenciais

- Use de Redux;
- Bom uso de componentes;
- Testes Unitários / Funcionais;
- Código bem comentado;
