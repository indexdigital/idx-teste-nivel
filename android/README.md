# IDX - Teste de Nível

O objetivo deste teste é analisar o seu conhecimento em desenvolvimento de aplicações utilizando componentes nativos e bibliotecas externas.

Como você pode ver no arquivo "App Diagram.png", o teste consiste em desenvolver um protótipo de aplicação de feed.

## Instruções Gerais

- [Duplique](https://help.github.com/articles/duplicating-a-repository/) este repositório (**não** faça um fork dele);
- Faça seu teste;
- Use este [JSON](https://idx.digital/static/idx-teste-de-nivel/mobile-test-one.json) para carregar por request;
- O layout segue em formato Sketch e PDF.

## Features

- Menu;
- Listagem do Feed;
- Favoritos;
- Feed Internal.

## O que esperamos

- Código limpo;
- Comentários quando você achar necessário explicar uma decisão ou deixar informações importantes a respeito da especificação;
- Uma forma simples de rodar o código e realizar os testes.

## Diferenciais

- Animações;
- Fidelidade ao layout original;
- Testes unitários.