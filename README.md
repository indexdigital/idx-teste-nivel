## Instruções Gerais

Obrigado por aplicar ao nosso teste. Faça o seu melhor para fazer parte da nossa equipe, desenvolvendo grandes projetos dos quais possamos nos orgulhar e falar para todo mundo!

A [Index](https://idx.digital) foi criada em 2006 em Fortaleza e agora está expandindo seu time para grandes projetos nacionais e procura novos membros para alçarmos vôos ainda maiores.

## Instruções Gerais

- [Duplique](https://help.github.com/articles/duplicating-a-repository/) este repositório (**não** faça um fork dele);
- Se você está aplicando para a vaga de back-end, por favor resolva os níveis em ordem crescente;
- Caso contrário, você pode acessar os devidos testes para Front-End, iOS ou Android;
- Sinta-se a vontade para realizar também os testes de outras plataformas para adquirir pontos extras;
- Commit pelo menos ao final de cada teste ou nível.

## O que esperamos

- Código limpo;
- Testes;
- Comentários quando você achar necessário explicar uma decisão ou deixar informações importantes a respeito da especificação;
- Uma forma simples de rodar o código e realizar os testes.

## Importante

Os testes de Android, iOS, Web Design e Front-end foram desenvolvidos em parceria com a equipe de uma de nossas empresas, enquanto os testes de backend foram ~~inspirados~~ copiados de testes de alguns de nossos clientes que já se provaram ser uma ótima forma de avaliar novas contratações. :)
